package com.cfperuweb.practicasesion01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText edtNombre, edtEdad;
    RadioButton rbPerro, rbGato, rbConejo;
    CheckBox chkVacuna, chkVacunaDos, chkVacunaTres;
    Button btnEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = findViewById(R.id.edtNombre);
        edtEdad = findViewById(R.id.edtEdad);
        rbPerro = findViewById(R.id.rbPerro);
        rbGato = findViewById(R.id.rbGato);
        rbConejo = findViewById(R.id.rbConejo);
        chkVacuna = findViewById(R.id.chkVacuna);
        chkVacunaDos = findViewById(R.id.chkVacunaDos);
        chkVacunaTres = findViewById(R.id.chkVacunaTres);
        btnEnviar = findViewById(R.id.btnEnviar);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nombre = edtNombre.getText().toString();
                String edad = edtEdad.getText().toString();

                if(nombre.isEmpty()){
                    Toast.makeText(MainActivity.this, "Debe ingresar el nombre de su mascota", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(edad.isEmpty()){
                    Toast.makeText(MainActivity.this, "Debe ingresar la edad de su mascota", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!chkVacuna.isChecked() && !chkVacunaDos.isChecked() && !chkVacunaTres.isChecked()){
                    Toast.makeText(MainActivity.this, "Debe seleccionar al menos una vacuna", Toast.LENGTH_SHORT).show();
                    return;
                }

                String mascota;
                if(rbPerro.isChecked()){
                    mascota = "Perro";
                }else if(rbGato.isChecked()){
                    mascota = "Gato";
                }else{
                    mascota = "Conejo";
                }


                Bundle bundle = new Bundle();
                bundle.putString("key_nombres", nombre);
                bundle.putString("key_edad", edad);
                bundle.putBoolean("key_vacuna", chkVacuna.isChecked() );
                bundle.putBoolean("key_vacuna_dos", chkVacunaDos.isChecked() );
                bundle.putBoolean("key_vacuna_tres", chkVacunaTres.isChecked() );
                bundle.putString("Key_mascota",mascota);

                Intent intent = new Intent(MainActivity.this, ResultadoActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }
}