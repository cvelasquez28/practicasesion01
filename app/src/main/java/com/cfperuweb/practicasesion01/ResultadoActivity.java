package com.cfperuweb.practicasesion01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity {

    TextView tvNombre, tvEdad, tvVacuna;
    ImageView ivMascota;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        tvNombre = findViewById(R.id.tvNombre);
        tvEdad = findViewById(R.id.tvEdad);
        tvVacuna = findViewById(R.id.tvVacuna);
        ivMascota = findViewById(R.id.ivMascota);

        String nombre =  getIntent().getStringExtra("key_nombres");;
        String edad = getIntent().getStringExtra("key_edad");
        boolean vacunaUno = getIntent().getBooleanExtra("key_vacuna", false);
        boolean vacunaDos = getIntent().getBooleanExtra("key_vacuna_dos", false);
        boolean vacunaTres = getIntent().getBooleanExtra("key_vacuna_tres", false);
        String mascota = getIntent().getStringExtra("Key_mascota");

        tvNombre.setText("Nombre: "+nombre);
        tvEdad.setText("Edad: "+edad);

        String cadena = "";

        if(vacunaUno){
            if(cadena.isEmpty()) cadena = "Vacuna 1";
            else cadena +=", Vacuna 1";
        }

        if(vacunaDos){
            if(cadena.isEmpty()) cadena = "Vacuna 2";
            else cadena +=", Vacuna 2";
        }

        if(vacunaTres){
            if(cadena.isEmpty()) cadena = "Vacuna 3";
            else cadena +=", Vacuna 3";
        }

        tvVacuna.setText("Vacunas: "+cadena);
        if(mascota.equals("Perro")){
            ivMascota.setImageDrawable(getResources().getDrawable(R.drawable.dog));
        }else if(mascota.equals("Gato")){
            ivMascota.setImageDrawable(getResources().getDrawable(R.drawable.cat));
        }else{
            ivMascota.setImageDrawable(getResources().getDrawable(R.drawable.rabbit));
        }
    }
}